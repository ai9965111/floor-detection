#install python packages
sudo apt-get install python3.7
sudo apt-get update -y
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
sudo update-alternatives --config python3
sudo apt install python3-pip
python -m pip install --upgrade --force-reinstall pip
python -m ensurepip
# !sudo apt-get install python3-distutils
sudo apt install python3.7-distutils

python3.7 -m pip uninstall -r ./scripts/old_versions.txt
python3.7 -m pip install -r ./scripts/requirements.txt

pip install ipykernel

#install mrcnn
sudo chmod +x ./scripts/install_mrcnn.sh
sh ./scripts/install_mrcnn.sh